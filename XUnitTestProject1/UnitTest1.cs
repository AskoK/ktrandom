using RandomNumbers;
using System;
using System.Collections.Generic;
using Xunit;
using System.Linq;


namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void CheckIfDuplicates()
        {
            // arrange
            const int expected = 1000;
           
            // act
            var randomNumbers = RandomNum.GenerateRandom(expected);
	        var actual = randomNumbers.Distinct().Count();
	        //Assert
	        Assert.Equal(expected, actual);

            //List<int> vals = RandomNum.GenerateRandom(1000);

            //Assert.True(vals.Count == vals.Distinct().Count());
        }
    }
}
