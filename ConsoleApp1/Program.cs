﻿using System;
using System.Collections.Generic;
using RandomNumbers;
using System.Linq;


namespace RandomNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> vals = RandomNum.GenerateRandom(1000);

            Console.WriteLine("Result: " + vals.Count);
            vals.ForEach(Console.WriteLine);

            if (vals.Count != vals.Distinct().Count())
            {
                // Duplicates exist
                Console.WriteLine("Boom");
            }
            else
            {
                Console.WriteLine("\n\nWorks!");
            }
            
            Console.ReadKey();
        }
    }
}
